# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:33+0000\n"
"PO-Revision-Date: 2022-12-11 14:20+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.0\n"

#: kcmtablet.cpp:32
#, kde-format
msgid "Primary (default)"
msgstr "Primary (standaard)"

#: kcmtablet.cpp:33
#, kde-format
msgid "Portrait"
msgstr "Portret"

#: kcmtablet.cpp:34
#, kde-format
msgid "Landscape"
msgstr "Landschap"

#: kcmtablet.cpp:35
#, kde-format
msgid "Inverted Portrait"
msgstr "Omgekeerd portret"

#: kcmtablet.cpp:36
#, kde-format
msgid "Inverted Landscape"
msgstr "Omgekeerd landschap"

#: kcmtablet.cpp:86
#, kde-format
msgid "Follow the active screen"
msgstr "Volg het actieve scherm"

#: kcmtablet.cpp:94
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: kcmtablet.cpp:137
#, kde-format
msgid "Fit to Output"
msgstr "In uitvoer laten passen"

#: kcmtablet.cpp:138
#, kde-format
msgid "Fit Output in tablet"
msgstr "Uitvoer in tablet laten passen"

#: kcmtablet.cpp:139
#, kde-format
msgid "Custom size"
msgstr "Aangepaste grootte"

#: package/contents/ui/main.qml:32
#, kde-format
msgid "No drawing tablets found."
msgstr "Geen tekentabletten gevonden."

#: package/contents/ui/main.qml:40
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Apparaat:"

#: package/contents/ui/main.qml:78
#, kde-format
msgid "Target display:"
msgstr "Doelbeeldscherm:"

#: package/contents/ui/main.qml:97
#, kde-format
msgid "Orientation:"
msgstr "Oriëntatie:"

#: package/contents/ui/main.qml:109
#, kde-format
msgid "Left-handed mode:"
msgstr "Modus linkshandig:"

#: package/contents/ui/main.qml:119
#, kde-format
msgid "Area:"
msgstr "Gebied:"

#: package/contents/ui/main.qml:207
#, kde-format
msgid "Resize the tablet area"
msgstr "Het tabletgebied van grootte wijzigen"

#: package/contents/ui/main.qml:231
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr "Beeldverhouding vergrendelen"

#: package/contents/ui/main.qml:239
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr "%1,%2 - %3×%4"

#: package/contents/ui/main.qml:247
#, kde-format
msgid "Tool Button 1"
msgstr "Hulpmiddelknop 1"

#: package/contents/ui/main.qml:248
#, kde-format
msgid "Tool Button 2"
msgstr "Hulpmiddelknop 2"

#: package/contents/ui/main.qml:249
#, kde-format
msgid "Tool Button 3"
msgstr "Hulpmiddelknop 3"

#: package/contents/ui/main.qml:292
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr "Blok:"

#: package/contents/ui/main.qml:303
#, kde-format
msgid "None"
msgstr "Geen"

#: package/contents/ui/main.qml:325
#, kde-format
msgid "Button %1:"
msgstr "Knop %1:"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Freek de Kruijf - 2021"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "freekdekruijf@kde.nl"

#~ msgid "Tablet"
#~ msgstr "Tablet"

#~ msgid "Configure drawing tablets"
#~ msgstr "Tekentabletten configureren"
