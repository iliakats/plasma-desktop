# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <qiilaq69@gmail.com>, 2016, 2019.
# Mihkel Tõnnov <mihhkel@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-11 00:46+0000\n"
"PO-Revision-Date: 2020-10-25 18:51+0100\n"
"Last-Translator: Mihkel Tõnnov <mihhkel@gmail.com>\n"
"Language-Team: Estonian <>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.2\n"

#: plasmasearch/kcm.cpp:87
#, kde-format
msgid "Available Plugins"
msgstr "Saadaolevad pluginad"

#: plasmasearch/package/contents/ui/main.qml:25
#, fuzzy, kde-format
#| msgid "Enable or disable plugins (used in KRunner and Application Launcher)"
msgid ""
"Enable or disable plugins (used in KRunner, Application Launcher, and the "
"Overview effect)"
msgstr "KRunneri ja rakenduste käivitaja pluginate lubamine või keelamine"

#: plasmasearch/package/contents/ui/main.qml:49
#, kde-format
msgid "Configure KRunner…"
msgstr ""

#: plasmasearch/package/contents/ui/main.qml:53
#, kde-format
msgid "Get New Plugins…"
msgstr ""

#: plugininstaller/AbstractJob.cpp:18
#, kde-kuit-format
msgctxt "@info:status"
msgid "Failed to run install script in terminal <message>%1</message>"
msgstr ""

#: plugininstaller/AbstractJob.cpp:29
#, kde-format
msgctxt "@info"
msgid "Installation executed successfully, you may now close this window"
msgstr ""

#: plugininstaller/AbstractJob.cpp:31
#, kde-format
msgctxt "@info"
msgid "Uninstallation executed successfully, you may now close this window"
msgstr ""

#: plugininstaller/main.cpp:31
#, kde-format
msgctxt "@info"
msgid "KRunner plugin installation failed"
msgstr ""

#: plugininstaller/main.cpp:43
#, kde-format
msgctxt "@info:shell"
msgid "Command to execute: install or uninstall."
msgstr ""

#: plugininstaller/main.cpp:44
#, kde-format
msgctxt "@info:shell"
msgid "Path to archive."
msgstr ""

#: plugininstaller/main.cpp:84
#, kde-format
msgctxt "@info"
msgid "No PackageKit support"
msgstr ""

#: plugininstaller/PackageKitConfirmationDialog.h:23
#: plugininstaller/ScriptConfirmationDialog.h:25
#, kde-format
msgctxt "@title:window"
msgid "Confirm Installation"
msgstr ""

#: plugininstaller/PackageKitConfirmationDialog.h:26
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You are about to install a binary package. You should only install these "
"from a trusted author/packager."
msgstr ""

#: plugininstaller/PackageKitConfirmationDialog.h:33
#: plugininstaller/ScriptConfirmationDialog.h:79
#, kde-format
msgctxt "@action:button"
msgid "Accept Risk And Continue"
msgstr ""

#: plugininstaller/PackageKitConfirmationDialog.h:37
#, kde-format
msgctxt "@action:button"
msgid "View File"
msgstr ""

#: plugininstaller/PackageKitJob.cpp:24
#, kde-format
msgctxt "@info"
msgid "The mime type %1 is not supported by the packagekit backend"
msgstr ""

#: plugininstaller/ScriptConfirmationDialog.h:32
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This plugin does not provide an uninstallation script. Please contact the "
"author. You can try to uninstall the plugin manually.<nl/>If you do not feel "
"capable or comfortable with this, click <interface>Cancel</interface>  now."
msgstr ""

#: plugininstaller/ScriptConfirmationDialog.h:37
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This plugin does not provide an uninstallation script. Please contact the "
"author. You can try to uninstall the plugin manually. Please have a look at "
"the README for instructions from the author.<nl/>If you do not feel capable "
"or comfortable with this, click <interface>Cancel</interface>  now."
msgstr ""

#: plugininstaller/ScriptConfirmationDialog.h:43
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This plugin does not provide an installation script. Please contact the "
"author. You can try to install the plugin manually.<nl/>If you do not feel "
"capable or comfortable with this, click <interface>Cancel</interface>  now."
msgstr ""

#: plugininstaller/ScriptConfirmationDialog.h:48
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This plugin does not provide an installation script. Please contact the "
"author. You can try to install the plugin manually. Please have a look at "
"the README for instructions from the author.<nl/>If you do not feel capable "
"or comfortable with this, click <interface>Cancel</interface>  now."
msgstr ""

#: plugininstaller/ScriptConfirmationDialog.h:54
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This plugin uses a script for installation which can pose a security risk. "
"Please examine the entire plugin's contents before installing, or at least "
"read the script's source code.<nl/>If you do not feel capable or comfortable "
"with this, click <interface>Cancel</interface>  now."
msgstr ""

#: plugininstaller/ScriptConfirmationDialog.h:60
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This plugin uses a script for installation which can pose a security risk. "
"Please examine the entire plugin's contents before installing, or at least "
"read the README file and the script's source code.<nl/>If you do not feel "
"capable or comfortable with this, click <interface>Cancel</interface>  now."
msgstr ""

#: plugininstaller/ScriptConfirmationDialog.h:75
#, kde-format
msgctxt "@action:button"
msgid "Mark entry as uninstalled"
msgstr ""

#: plugininstaller/ScriptConfirmationDialog.h:77
#, kde-format
msgctxt "@action:button"
msgid "Mark entry as installed"
msgstr ""

#: plugininstaller/ScriptConfirmationDialog.h:85
#, kde-format
msgctxt "@action:button"
msgid "View Script"
msgstr ""

#: plugininstaller/ScriptConfirmationDialog.h:91
#, kde-format
msgctxt "@action:button"
msgid "View Source Directory"
msgstr ""

#: plugininstaller/ScriptConfirmationDialog.h:102
#, kde-format
msgctxt "@action:button"
msgid "View %1"
msgstr ""

#: plugininstaller/ZypperRPMJob.cpp:30
#, kde-format
msgctxt "@info"
msgid "Could not resolve package name of %1"
msgstr ""

#: plugininstaller/ZypperRPMJob.cpp:37
#, kde-kuit-format
msgctxt "@info"
msgid "Failed to run install command: <message>%1</message>"
msgstr ""

#~ msgid "Position on screen:"
#~ msgstr "Asukoht ekraanil:"

#~ msgid "Top"
#~ msgstr "Ülal"

#~ msgid "Center"
#~ msgstr "Keskel"

#~ msgid "History:"
#~ msgstr "Ajalugu:"

#~ msgid "Clear History"
#~ msgstr "Puhasta ajalugu"

#, fuzzy
#~| msgid "Clear History"
#~ msgid "Clear History…"
#~ msgstr "Puhasta ajalugu"

#, fuzzy
#~| msgctxt "kcm name for About dialog"
#~| msgid "Configure Search Bar"
#~ msgctxt "@action:button"
#~ msgid "Configure Enabled Search Plugins…"
#~ msgstr "Otsinguriba seadistamine"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Marek Laane"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "qiilaq69@gmail.com"

#, fuzzy
#~| msgctxt "kcm name for About dialog"
#~| msgid "Configure Search Bar"
#~ msgctxt "kcm name for About dialog"
#~ msgid "Configure search settings"
#~ msgstr "Otsinguriba seadistamine"

#~ msgid "Vishesh Handa"
#~ msgstr "Vishesh Handa"

#~ msgid "Enable"
#~ msgstr "Lubatud"

#, fuzzy
#~| msgid "Clear History"
#~ msgid "KRunner history:"
#~ msgstr "Puhasta ajalugu"

#~ msgid "Retain previous search"
#~ msgstr "Eelmise otsingu säilitamine"

#, fuzzy
#~| msgid "Clear History"
#~ msgid "Clear History..."
#~ msgstr "Puhasta ajalugu"

#~ msgid "Enable or disable KRunner plugins:"
#~ msgstr "KRunneri pluginate lubamine või keelamine:"

#, fuzzy
#~| msgid "Select the search plugins"
#~ msgid "Select the search plugins:"
#~ msgstr "Otsingupluginate valimine"
