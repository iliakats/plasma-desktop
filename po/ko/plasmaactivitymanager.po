# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Shinjo Park <kde@peremen.name>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-08 00:48+0000\n"
"PO-Revision-Date: 2016-10-20 23:08+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 2.0\n"

#: sortedactivitiesmodel.cpp:311
#, kde-format
msgid "Used some time ago"
msgstr "오래 전에 사용됨"

#: sortedactivitiesmodel.cpp:327
#, kde-format
msgid "Used more than a year ago"
msgstr "1년 이전에 사용됨"

#: sortedactivitiesmodel.cpp:328
#, kde-format
msgctxt "amount in months"
msgid "Used a month ago"
msgid_plural "Used %1 months ago"
msgstr[0] "%1개월 전에 사용됨"

#: sortedactivitiesmodel.cpp:329
#, kde-format
msgctxt "amount in days"
msgid "Used a day ago"
msgid_plural "Used %1 days ago"
msgstr[0] "%1일 전에 사용됨"

#: sortedactivitiesmodel.cpp:330
#, kde-format
msgctxt "amount in hours"
msgid "Used an hour ago"
msgid_plural "Used %1 hours ago"
msgstr[0] "%1시간 전에 사용됨"

#: sortedactivitiesmodel.cpp:331
#, kde-format
msgctxt "amount in minutes"
msgid "Used a minute ago"
msgid_plural "Used %1 minutes ago"
msgstr[0] "%1분 전에 사용됨"

#: sortedactivitiesmodel.cpp:332
#, kde-format
msgid "Used a moment ago"
msgstr "잠시 전에 사용됨"

#: switcherbackend.cpp:167
#, kde-format
msgid "Walk through activities"
msgstr "활동간 전환"

#: switcherbackend.cpp:172
#, kde-format
msgid "Walk through activities (Reverse)"
msgstr "활동간 전환(거꾸로)"
