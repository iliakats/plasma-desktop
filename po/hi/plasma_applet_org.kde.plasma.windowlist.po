# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Raghavendra Kamath <raghu@raghukamath.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-29 00:59+0000\n"
"PO-Revision-Date: 2021-10-11 11:39+0530\n"
"Last-Translator: Raghavendra Kamath <raghu@raghukamath.com>\n"
"Language-Team: kde-hindi\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 21.08.2\n"

#: contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr ""

#: contents/ui/ConfigGeneral.qml:27
#, kde-format
msgid "Show active application's name on Panel button"
msgstr ""

#: contents/ui/ConfigGeneral.qml:47
#, kde-format
msgid "Only icons can be shown when the Panel is vertical."
msgstr ""

#: contents/ui/ConfigGeneral.qml:49
#, kde-format
msgid "Not applicable when the widget is on the Desktop."
msgstr ""

#: contents/ui/main.qml:86
#, kde-format
msgid "Plasma Desktop"
msgstr ""

#~ msgid "Show list of opened windows"
#~ msgstr "खुली हुई खिड़कियों की सूची दिखाएं"

#~ msgid "On all desktops"
#~ msgstr "सभी डेस्कटॉप पर"
