msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-14 00:48+0000\n"
"PO-Revision-Date: 2023-02-24 12:43\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/plasma-desktop/plasma_applet_org.kde."
"panel.pot\n"
"X-Crowdin-File-ID: 7465\n"

#: contents/ui/ConfigOverlay.qml:260 contents/ui/ConfigOverlay.qml:295
#, kde-format
msgid "Remove"
msgstr "移除"

#: contents/ui/ConfigOverlay.qml:270
#, kde-format
msgid "Configure…"
msgstr "配置…"

#: contents/ui/ConfigOverlay.qml:280
#, kde-format
msgid "Show Alternatives…"
msgstr "显示替代方案…"

#: contents/ui/ConfigOverlay.qml:305
#, kde-format
msgid "Spacer width"
msgstr "空隔宽度"
