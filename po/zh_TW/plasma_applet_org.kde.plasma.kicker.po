# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Franklin, 2014, 2015.
# Jeff Huang <s8321414@gmail.com>, 2016, 2017.
# pan93412 <pan93412@gmail.com>, 2018, 2019.
# Chaoting Liu <brli@chakralinux.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-12 02:13+0000\n"
"PO-Revision-Date: 2021-09-06 01:14+0800\n"
"Last-Translator: Chaoting Liu <brli@chakralinux.org>\n"
"Language-Team: Chinese <kde-i18n-doc@kde.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.08.1\n"

#: package/contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "一般"

#: package/contents/ui/code/tools.js:42
#, kde-format
msgid "Remove from Favorites"
msgstr "從我的最愛中移除"

#: package/contents/ui/code/tools.js:46
#, kde-format
msgid "Add to Favorites"
msgstr "新增到我的最愛"

#: package/contents/ui/code/tools.js:70
#, kde-format
msgid "On All Activities"
msgstr "所有活動"

#: package/contents/ui/code/tools.js:120
#, kde-format
msgid "On the Current Activity"
msgstr "在目前的活動"

#: package/contents/ui/code/tools.js:134
#, kde-format
msgid "Show in Favorites"
msgstr "在我的最愛顯示"

#: package/contents/ui/ConfigGeneral.qml:45
#, kde-format
msgid "Icon:"
msgstr "圖示："

#: package/contents/ui/ConfigGeneral.qml:125
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr "選擇…"

#: package/contents/ui/ConfigGeneral.qml:130
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "清除圖示"

#: package/contents/ui/ConfigGeneral.qml:148
#, kde-format
msgid "Show applications as:"
msgstr "顯示應用程式方式："

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgid "Name only"
msgstr "只有名稱"

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgid "Description only"
msgstr "只有描述"

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgid "Name (Description)"
msgstr "名稱（描述）"

#: package/contents/ui/ConfigGeneral.qml:150
#, kde-format
msgid "Description (Name)"
msgstr "描述（名稱）"

#: package/contents/ui/ConfigGeneral.qml:160
#, kde-format
msgid "Behavior:"
msgstr "行為："

#: package/contents/ui/ConfigGeneral.qml:162
#, kde-format
msgid "Sort applications alphabetically"
msgstr "依應用程式字母排序"

#: package/contents/ui/ConfigGeneral.qml:170
#, kde-format
msgid "Flatten sub-menus to a single level"
msgstr "將子選單攤平成一個階級"

#: package/contents/ui/ConfigGeneral.qml:178
#, kde-format
msgid "Show icons on the root level of the menu"
msgstr "在選單最上層顯示圖示"

#: package/contents/ui/ConfigGeneral.qml:188
#, kde-format
msgid "Show categories:"
msgstr "顯示類別："

#: package/contents/ui/ConfigGeneral.qml:191
#, kde-format
msgid "Recent applications"
msgstr "最近使用的應用程式"

#: package/contents/ui/ConfigGeneral.qml:192
#, kde-format
msgid "Often used applications"
msgstr "經常使用的應用程式"

#: package/contents/ui/ConfigGeneral.qml:199
#, kde-format
msgid "Recent files"
msgstr "最近使用的文件"

#: package/contents/ui/ConfigGeneral.qml:200
#, kde-format
msgid "Often used files"
msgstr "經常使用的檔案"

#: package/contents/ui/ConfigGeneral.qml:206
#, kde-format
msgid "Sort items in categories by:"
msgstr "類別項目排序方式："

#: package/contents/ui/ConfigGeneral.qml:207
#, kde-format
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Often used]"
msgid "Recently used"
msgstr "最近使用"

#: package/contents/ui/ConfigGeneral.qml:207
#, kde-format
msgctxt ""
"@item:inlistbox Sort items in categories by [Recently used | Ofetn used]"
msgid "Often used"
msgstr "經常使用"

#: package/contents/ui/ConfigGeneral.qml:217
#, kde-format
msgid "Search:"
msgstr "搜尋："

#: package/contents/ui/ConfigGeneral.qml:219
#, kde-format
msgid "Expand search to bookmarks, files and emails"
msgstr "將搜尋擴展到書籤、檔案與電子郵件"

#: package/contents/ui/ConfigGeneral.qml:227
#, kde-format
msgid "Align search results to bottom"
msgstr "將搜尋結果置於底部"

#: package/contents/ui/DashboardRepresentation.qml:288
#, kde-format
msgid "Searching for '%1'"
msgstr "正在搜尋「%1」"

#: package/contents/ui/DashboardRepresentation.qml:288
#, fuzzy, kde-format
#| msgid "Type to search…"
msgctxt "@info:placeholder as in, 'start typing to initiate a search'"
msgid "Type to search…"
msgstr "輸入以搜尋…"

#: package/contents/ui/DashboardRepresentation.qml:388
#, kde-format
msgid "Favorites"
msgstr "我的最愛"

#: package/contents/ui/DashboardRepresentation.qml:606
#: package/contents/ui/DashboardTabBar.qml:42
#, kde-format
msgid "Widgets"
msgstr "元件"

#: package/contents/ui/DashboardTabBar.qml:31
#, kde-format
msgid "Apps & Docs"
msgstr "應用程式與文件"

#: package/contents/ui/main.qml:254
#, kde-format
msgid "Edit Applications…"
msgstr "編輯應用程式…"

#~ msgid "Recent contacts"
#~ msgstr "最近使用的聯絡人"

#~ msgid "Often used contacts"
#~ msgstr "經常使用的聯絡人"

#~ msgid "Search…"
#~ msgstr "搜尋："

#~ msgid "Search..."
#~ msgstr "搜尋..."

#~ msgid "Often used documents"
#~ msgstr "經常使用的文件"
