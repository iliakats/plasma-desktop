# translation of kcminput.po to Maithili
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sangeeta Kumari <sangeeta09@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kcminput\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:33+0000\n"
"PO-Revision-Date: 2009-01-18 15:28+0530\n"
"Last-Translator: Sangeeta Kumari <sangeeta09@gmail.com>\n"
"Language-Team: Maithili <maithili.sf.net>\n"
"Language: mai\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"\n"
"\n"
"X-Generator: KBabel 1.11.4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "संगीता कुमारी"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "sangeeta09@gmail.com"

#: backends/kwin_wl/kwin_wl_backend.cpp:66
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""

#: backends/kwin_wl/kwin_wl_backend.cpp:86
#, kde-format
msgid "Critical error on reading fundamental device infos of %1."
msgstr ""

#: kcm/libinput/libinput_config.cpp:90
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:95
#, kde-format
msgid "No pointer device found. Connect now."
msgstr ""

#: kcm/libinput/libinput_config.cpp:106
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""

#: kcm/libinput/libinput_config.cpp:126
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""

#: kcm/libinput/libinput_config.cpp:148
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""

#: kcm/libinput/libinput_config.cpp:172
#, kde-format
msgid "Pointer device disconnected. Closed its setting dialog."
msgstr ""

#: kcm/libinput/libinput_config.cpp:174
#, kde-format
msgid "Pointer device disconnected. No other devices found."
msgstr ""

#: kcm/libinput/main.qml:74
#, kde-format
msgid "Device:"
msgstr ""

#: kcm/libinput/main.qml:98 kcm/libinput/main_deviceless.qml:49
#, fuzzy, kde-format
#| msgid "&General"
msgid "General:"
msgstr "सामान्य (&G)"

#: kcm/libinput/main.qml:100
#, kde-format
msgid "Device enabled"
msgstr ""

#: kcm/libinput/main.qml:120
#, kde-format
msgid "Accept input through this device."
msgstr ""

#: kcm/libinput/main.qml:125 kcm/libinput/main_deviceless.qml:51
#, fuzzy, kde-format
#| msgid "Le&ft handed"
msgid "Left handed mode"
msgstr "बम्माँ हाथ बला (&f)"

#: kcm/libinput/main.qml:145 kcm/libinput/main_deviceless.qml:71
#, kde-format
msgid "Swap left and right buttons."
msgstr ""

#: kcm/libinput/main.qml:150 kcm/libinput/main_deviceless.qml:76
#, kde-format
msgid "Press left and right buttons for middle-click"
msgstr ""

#: kcm/libinput/main.qml:170 kcm/libinput/main_deviceless.qml:96
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""

#: kcm/libinput/main.qml:179 kcm/libinput/main_deviceless.qml:105
#, fuzzy, kde-format
#| msgid "Pointer threshold:"
msgid "Pointer speed:"
msgstr "सूचक सीमाः"

#: kcm/libinput/main.qml:211 kcm/libinput/main_deviceless.qml:137
#, fuzzy, kde-format
#| msgid "Acceleration &profile:"
msgid "Acceleration profile:"
msgstr "त्वरण प्रोफाइलः (&p)"

#: kcm/libinput/main.qml:242 kcm/libinput/main_deviceless.qml:168
#, kde-format
msgid "Flat"
msgstr ""

#: kcm/libinput/main.qml:246 kcm/libinput/main_deviceless.qml:172
#, kde-format
msgid "Cursor moves the same distance as the mouse movement."
msgstr ""

#: kcm/libinput/main.qml:252 kcm/libinput/main_deviceless.qml:178
#, kde-format
msgid "Adaptive"
msgstr ""

#: kcm/libinput/main.qml:256 kcm/libinput/main_deviceless.qml:182
#, kde-format
msgid "Cursor travel distance depends on the mouse movement speed."
msgstr ""

#: kcm/libinput/main.qml:267 kcm/libinput/main_deviceless.qml:193
#, kde-format
msgid "Scrolling:"
msgstr ""

#: kcm/libinput/main.qml:269 kcm/libinput/main_deviceless.qml:195
#, fuzzy, kde-format
#| msgid "Re&verse scroll direction"
msgid "Invert scroll direction"
msgstr "विपरीत स्क्रोल दिशा (&v)"

#: kcm/libinput/main.qml:285 kcm/libinput/main_deviceless.qml:211
#, kde-format
msgid "Touchscreen like scrolling."
msgstr ""

#: kcm/libinput/main.qml:290
#, fuzzy, kde-format
#| msgid "Pointer threshold:"
msgid "Scrolling speed:"
msgstr "सूचक सीमाः"

#: kcm/libinput/main.qml:338
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr ""

#: kcm/libinput/main.qml:344
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr ""

#: kcm/libinput/main.qml:355
#, kde-format
msgctxt "@action:button"
msgid "Re-bind Additional Mouse Buttons…"
msgstr ""

#: kcm/libinput/main.qml:391
#, kde-format
msgctxt "@label for assigning an action to a numbered button"
msgid "Extra Button %1:"
msgstr ""

#: kcm/libinput/main.qml:421
#, kde-format
msgctxt "@action:button"
msgid "Press the mouse button for which you want to add a key binding"
msgstr ""

#: kcm/libinput/main.qml:422
#, kde-format
msgctxt "@action:button, %1 is the translation of 'Extra Button %1' from above"
msgid "Enter the new key combination for %1"
msgstr ""

#: kcm/libinput/main.qml:426
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr ""

#: kcm/libinput/main.qml:443
#, fuzzy, kde-format
#| msgid "Press Connect Button"
msgctxt "@action:button"
msgid "Press a mouse button "
msgstr "कनेक्ट बटन केँ दबाबू"

#: kcm/libinput/main.qml:444
#, kde-format
msgctxt "@action:button, Bind a mousebutton to keyboard key(s)"
msgid "Add Binding…"
msgstr ""

#: kcm/libinput/main.qml:473
#, kde-format
msgctxt "@action:button"
msgid "Go back"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QWidget, KCMMouse)
#: kcm/xlib/kcmmouse.ui:14
#, kde-format
msgid ""
"<h1>Mouse</h1> This module allows you to choose various options for the way "
"in which your pointing device works. Your pointing device may be a mouse, "
"trackball, or some other hardware that performs a similar function."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, generalTab)
#: kcm/xlib/kcmmouse.ui:36
#, kde-format
msgid "&General"
msgstr "सामान्य (&G)"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:52
#, kde-format
msgid ""
"If you are left-handed, you may prefer to swap the functions of the left and "
"right buttons on your pointing device by choosing the 'left-handed' option. "
"If your pointing device has more than two buttons, only those that function "
"as the left and right buttons are affected. For example, if you have a three-"
"button mouse, the middle button is unaffected."
msgstr ""

#. i18n: ectx: property (title), widget (QGroupBox, handedBox)
#: kcm/xlib/kcmmouse.ui:55
#, kde-format
msgid "Button Order"
msgstr "बटन क्रम"

#. i18n: ectx: property (text), widget (QRadioButton, rightHanded)
#: kcm/xlib/kcmmouse.ui:64
#, kde-format
msgid "Righ&t handed"
msgstr "दहिन्ना हाथबला (&t)"

#. i18n: ectx: property (text), widget (QRadioButton, leftHanded)
#: kcm/xlib/kcmmouse.ui:77
#, kde-format
msgid "Le&ft handed"
msgstr "बम्माँ हाथ बला (&f)"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:106
#, kde-format
msgid ""
"Change the direction of scrolling for the mouse wheel or the 4th and 5th "
"mouse buttons."
msgstr "माउस व्हील केर स्क्रालिंग अथवा 4था आओर 5वाँ माउस बटनक दिशा बदलू."

#. i18n: ectx: property (text), widget (QCheckBox, cbScrollPolarity)
#: kcm/xlib/kcmmouse.ui:109
#, kde-format
msgid "Re&verse scroll direction"
msgstr "विपरीत स्क्रोल दिशा (&v)"

#. i18n: ectx: attribute (title), widget (QWidget, advancedTab)
#: kcm/xlib/kcmmouse.ui:156
#, kde-format
msgid "Advanced"
msgstr "उन्नत"

#. i18n: ectx: property (text), widget (QLabel, label)
#: kcm/xlib/kcmmouse.ui:164
#, kde-format
msgid "Pointer acceleration:"
msgstr "सूचक त्वरणः"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: kcm/xlib/kcmmouse.ui:174
#, kde-format
msgid "Pointer threshold:"
msgstr "सूचक सीमाः"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: kcm/xlib/kcmmouse.ui:184
#, kde-format
msgid "Double click interval:"
msgstr "डबल क्लिक अंतरालः"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: kcm/xlib/kcmmouse.ui:194
#, kde-format
msgid "Drag start time:"
msgstr "ड्रेग प्रारंभ समयः"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: kcm/xlib/kcmmouse.ui:204
#, kde-format
msgid "Drag start distance:"
msgstr "ड्रेग प्रारंभ दूरीः"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: kcm/xlib/kcmmouse.ui:214
#, kde-format
msgid "Mouse wheel scrolls by:"
msgstr "माउस व्हील स्क्रोल करताह:"

#. i18n: ectx: property (whatsThis), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:230
#, kde-format
msgid ""
"<p>This option allows you to change the relationship between the distance "
"that the mouse pointer moves on the screen and the relative movement of the "
"physical device itself (which may be a mouse, trackball, or some other "
"pointing device.)</p><p> A high value for the acceleration will lead to "
"large movements of the mouse pointer on the screen even when you only make a "
"small movement with the physical device. Selecting very high values may "
"result in the mouse pointer flying across the screen, making it hard to "
"control.</p>"
msgstr ""

#. i18n: ectx: property (suffix), widget (QDoubleSpinBox, accel)
#: kcm/xlib/kcmmouse.ui:233
#, kde-format
msgid " x"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, thresh)
#: kcm/xlib/kcmmouse.ui:261
#, kde-format
msgid ""
"<p>The threshold is the smallest distance that the mouse pointer must move "
"on the screen before acceleration has any effect. If the movement is smaller "
"than the threshold, the mouse pointer moves as if the acceleration was set "
"to 1X;</p><p> thus, when you make small movements with the physical device, "
"there is no acceleration at all, giving you a greater degree of control over "
"the mouse pointer. With larger movements of the physical device, you can "
"move the mouse pointer rapidly to different areas on the screen.</p>"
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, doubleClickInterval)
#: kcm/xlib/kcmmouse.ui:280
#, kde-format
msgid ""
"The double click interval is the maximal time (in milliseconds) between two "
"mouse clicks which turns them into a double click. If the second click "
"happens later than this time interval after the first click, they are "
"recognized as two separate clicks."
msgstr ""

#. i18n: ectx: property (suffix), widget (QSpinBox, doubleClickInterval)
#. i18n: ectx: property (suffix), widget (QSpinBox, dragStartTime)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_delay)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_interval)
#. i18n: ectx: property (suffix), widget (QSpinBox, mk_time_to_max)
#: kcm/xlib/kcmmouse.ui:283 kcm/xlib/kcmmouse.ui:311 kcm/xlib/kcmmouse.ui:408
#: kcm/xlib/kcmmouse.ui:450 kcm/xlib/kcmmouse.ui:482
#, kde-format
msgid " msec"
msgstr "मि.से."

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartTime)
#: kcm/xlib/kcmmouse.ui:308
#, kde-format
msgid ""
"If you click with the mouse (e.g. in a multi-line editor) and begin to move "
"the mouse within the drag start time, a drag operation will be initiated."
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, dragStartDist)
#: kcm/xlib/kcmmouse.ui:333
#, kde-format
msgid ""
"If you click with the mouse and begin to move the mouse at least the drag "
"start distance, a drag operation will be initiated."
msgstr ""

#. i18n: ectx: property (whatsThis), widget (QSpinBox, wheelScrollLines)
#: kcm/xlib/kcmmouse.ui:355
#, kde-format
msgid ""
"If you use the wheel of a mouse, this value determines the number of lines "
"to scroll for each wheel movement. Note that if this number exceeds the "
"number of visible lines, it will be ignored and the wheel movement will be "
"handled as a page up/down movement."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, MouseNavigation)
#: kcm/xlib/kcmmouse.ui:387
#, fuzzy, kde-format
#| msgid "Mouse Navigation"
msgid "Keyboard Navigation"
msgstr "माउस नेविगेशन"

#. i18n: ectx: property (text), widget (QCheckBox, mouseKeys)
#: kcm/xlib/kcmmouse.ui:395
#, kde-format
msgid "&Move pointer with keyboard (using the num pad)"
msgstr "कुंजीपट केर सँग सूचक केँ घसकाबू (न्यूमेरिक पैड केर उपयोग सँ) (&M)"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: kcm/xlib/kcmmouse.ui:424
#, kde-format
msgid "&Acceleration delay:"
msgstr "त्वरण देरीः (&A)"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: kcm/xlib/kcmmouse.ui:434
#, kde-format
msgid "R&epeat interval:"
msgstr "दोहराबै क' अंतराल (&e):"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: kcm/xlib/kcmmouse.ui:466
#, kde-format
msgid "Acceleration &time:"
msgstr "त्वरण समयः (&t)"

#. i18n: ectx: property (text), widget (QLabel, label_10)
#: kcm/xlib/kcmmouse.ui:498
#, kde-format
msgid "Ma&ximum speed:"
msgstr "अधिकतम गतिः (&x)"

#. i18n: ectx: property (suffix), widget (QSpinBox, mk_max_speed)
#: kcm/xlib/kcmmouse.ui:514
#, kde-format
msgid " pixel/sec"
msgstr "पिक्सेल्स/सेक"

#. i18n: ectx: property (text), widget (QLabel, label_11)
#: kcm/xlib/kcmmouse.ui:530
#, kde-format
msgid "Acceleration &profile:"
msgstr "त्वरण प्रोफाइलः (&p)"

#: kcm/xlib/xlib_config.cpp:267 kcm/xlib/xlib_config.cpp:272
#, kde-format
msgid " pixel"
msgid_plural " pixels"
msgstr[0] "पिक्सेल"
msgstr[1] "पिक्सेल"

#: kcm/xlib/xlib_config.cpp:277
#, kde-format
msgid " line"
msgid_plural " lines"
msgstr[0] "पंक्ति"
msgstr[1] "पंक्ति"

#~ msgid "Mouse"
#~ msgstr "माउस"

#, fuzzy
#~| msgid "(c) 1997 - 2005 Mouse developers"
#~ msgid "(c) 1997 - 2018 Mouse developers"
#~ msgstr "(c) 1997 - 2005 माउस डेवलपर्स"

#~ msgid "Patrick Dowler"
#~ msgstr "पेट्रिक डाउलर"

#~ msgid "Dirk A. Mueller"
#~ msgstr "डिर्क ए. मुलर"

#~ msgid "David Faure"
#~ msgstr "डेविड फाउरे"

#~ msgid "Bernd Gehrmann"
#~ msgstr "बेर्न्द गेहर्मन"

#~ msgid "Rik Hemsley"
#~ msgstr "रिक हेम्सले"

#~ msgid "Brad Hughes"
#~ msgstr "ब्रेज हजेस"

#~ msgid "Brad Hards"
#~ msgstr "ब्रेड हार्ड्स"

#~ msgid "Ralf Nolden"
#~ msgstr "राल्फ नाल्देन"

#, fuzzy
#~| msgid "Acceleration &time:"
#~ msgid "Acceleration:"
#~ msgstr "त्वरण समयः (&t)"

#, fuzzy
#~| msgid "Acceleration &profile:"
#~ msgid "Acceleration Profile:"
#~ msgstr "त्वरण प्रोफाइलः (&p)"

#~ msgid "Icons"
#~ msgstr "प्रतीक"

#~ msgid ""
#~ "Dou&ble-click to open files and folders (select icons on first click)"
#~ msgstr ""
#~ "फाइल आओर फोल्डर केँ खोलबा क' लेल डबल क्लिक करू (पहिल क्लिक पर प्रतीक केँ चयन करू) "
#~ "(&b)"

#~ msgid "Activates and opens a file or folder with a single click."
#~ msgstr "एकटा क्लिक सँ फाइल अथवा फोल्डर केँ सक्रिय करैत अछि आओर खोलैत अछि."

#~ msgid "&Single-click to open files and folders"
#~ msgstr "फाइल आओर फोल्डर केँ खोलबा क' लेल सिंगल क्लिक (&S)"

#~ msgid "Select the cursor theme you want to use:"
#~ msgstr "संकेतक प्रसंग चुनू जकर उपयोग अहाँ कएनाइ चाहैत छी:"

#~ msgid "Name"
#~ msgstr "नाम"

#~ msgid "Description"
#~ msgstr "विवरण"

#~ msgid "You have to restart KDE for these changes to take effect."
#~ msgstr "परिवर्तनसभ केँ प्रभाव मे लाबै क' लेल अहाँक केडीई केँ फिनु सँ प्रारंभ कएनाइ पड़त."

#~ msgid "Cursor Settings Changed"
#~ msgstr "संकेतक बिन्यास परिवर्तित"

#~ msgid "Small black"
#~ msgstr "छोट करिया"

#~ msgid "Small black cursors"
#~ msgstr "छोट करिया संकेतक"

#~ msgid "Large black"
#~ msgstr "पैघ करिआ"

#~ msgid "Large black cursors"
#~ msgstr "पैघ करिया संकेतक"

#~ msgid "Small white"
#~ msgstr "छोट उज्जर"

#~ msgid "Small white cursors"
#~ msgstr "छोट उज्जर संकेतक"

#~ msgid "Large white"
#~ msgstr "पैघ उज्जर"

#~ msgid "Large white cursors"
#~ msgstr "पैघ उज्जर संकेतक"

#~ msgid "Cha&nge pointer shape over icons"
#~ msgstr "प्रतीकसभ केर उप्पर आबै पर सूचक क' सकल बदलू (&n)"

#~ msgid "A&utomatically select icons"
#~ msgstr "प्रतीकसभ केँ स्वचलित चुनू (&u)"

#, fuzzy
#~| msgid "Dela&y:"
#~ msgctxt "label. delay (on milliseconds) to automatically select icons"
#~ msgid "Delay"
#~ msgstr "देरी: (&y)"

#, fuzzy
#~| msgid " msec"
#~ msgctxt "milliseconds. time to automatically select the items"
#~ msgid " ms"
#~ msgstr "मि.से."

#~ msgid "Mouse type: %1"
#~ msgstr "माउस प्रकार: %1"

#~ msgid ""
#~ "RF channel 1 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "आरएफ चैनल 1 नियत कएल गेल अछि. लिंक फिनु सँ स्थापित करब क' लेल कृप्या माउस पर कनेक्ट "
#~ "बटन दबाबू."

#~ msgid ""
#~ "RF channel 2 has been set. Please press Connect button on mouse to re-"
#~ "establish link"
#~ msgstr ""
#~ "आरएफ चैनल 2 नियत कएल गेल अछि. लिंक फिनु सँ स्थापित करब क' लेल कृप्या माउस पर कनेक्ट "
#~ "बटन दबाबू."

#, fuzzy
#~| msgid "none"
#~ msgctxt "no cordless mouse"
#~ msgid "none"
#~ msgstr "किछु नहि"

#~ msgid "Cordless Mouse"
#~ msgstr "कार्डलेस माउस"

#~ msgid "Cordless Wheel Mouse"
#~ msgstr "कार्डलैस व्हील माउस"

#~ msgid "Cordless MouseMan Wheel"
#~ msgstr "कार्डलेस माउस-मेन व्हील"

#~ msgid "Cordless TrackMan Wheel"
#~ msgstr "कार्डलेस ट्रैक-मेन व्हील"

#~ msgid "TrackMan Live"
#~ msgstr "ट्रैक-मेन लाइव"

#~ msgid "Cordless TrackMan FX"
#~ msgstr "कार्डलेस ट्रैक-मेन एफएक्स"

#~ msgid "Cordless MouseMan Optical"
#~ msgstr "कार्डलेस आप्टिकल माउस-मेन"

#~ msgid "Cordless Optical Mouse"
#~ msgstr "कार्डलेस आप्टिकल माउस"

#~ msgid "Cordless MouseMan Optical (2ch)"
#~ msgstr "कार्डलेस आप्टिकल माउस (2 चै)"

#~ msgid "Cordless Optical Mouse (2ch)"
#~ msgstr "कार्डलेस आप्टिकल माउस (2 चै)"

#~ msgid "Cordless Mouse (2ch)"
#~ msgstr "कार्डलेस माउस (2चै)"

#~ msgid "Cordless Optical TrackMan"
#~ msgstr "कार्डलेस आप्टिकल ट्रैक-मेन"

#~ msgid "MX700 Cordless Optical Mouse"
#~ msgstr "एमएक्स700 कार्डलेस आप्टिकल माउस"

#~ msgid "MX700 Cordless Optical Mouse (2ch)"
#~ msgstr "एमएक्स700 कार्डलेस आप्टिकल माउस (2 चै)"

#~ msgid "Unknown mouse"
#~ msgstr "अज्ञात माउस"

#~ msgid "Cordless Name"
#~ msgstr "कार्डलेस नाम"

#~ msgid "Sensor Resolution"
#~ msgstr "सेंसर रेजोल्यूशन"

#~ msgid "400 counts per inch"
#~ msgstr "400 गणना प्रति इंच"

#~ msgid "800 counts per inch"
#~ msgstr "800 गणना प्रति इंच"

#~ msgid "Battery Level"
#~ msgstr "बैटरी स्तर"

#~ msgid "RF Channel"
#~ msgstr "आरएफ चैनल"

#~ msgid "Channel 1"
#~ msgstr "चैनल 1"

#~ msgid "Channel 2"
#~ msgstr "चैनल 2"

#, fuzzy
#~| msgid "&Cursor Theme"
#~ msgid "Cursor Theme"
#~ msgstr "संकेतक प्रसंग (&C)"

#~ msgid "Drag or Type Theme URL"
#~ msgstr "प्रसंग यूआरएल घींचू अथवा टाइप करू"

#~ msgid "Unable to find the cursor theme archive %1."
#~ msgstr "संकेतक प्रसंग अभिलेख %1 पाबै मे अक्षम."

#~ msgid ""
#~ "Unable to download the cursor theme archive; please check that the "
#~ "address %1 is correct."
#~ msgstr "संकेतक प्रसंग अभिलेख डाउनलोड करबा मे अक्षम; कृप्या जाँचें जे पता %1 सही अछि."

#~ msgid "The file %1 does not appear to be a valid cursor theme archive."
#~ msgstr "फाइल %1 वैध संकेतक प्रसंग अभिलेखागार जहिना प्रतीत नहि अछि."

#~ msgid "Confirmation"
#~ msgstr "पुष्टिकरण"

#~ msgid ""
#~ "A theme named %1 already exists in your icon theme folder. Do you want "
#~ "replace it with this one?"
#~ msgstr ""
#~ "एकटा प्रसंग %1 नाम सँ प्रतीक प्रसंग फोल्डर मे पहिने सँ उपलब्ध अछि. की अहाँ ओकरा एकरासँ "
#~ "बदलब चाहैत अछि?"

#~ msgid "Overwrite Theme?"
#~ msgstr "प्रसंग मेटाकए लिखू?"

#~ msgid ""
#~ "Select the cursor theme you want to use (hover preview to test cursor):"
#~ msgstr ""
#~ "संकेतक प्रसंग जे अहाँ उपयोग कएनाइ चाहैत छी ओकरा चुनू (संकेतक केँ जाँचबाक क' लेल "
#~ "पूर्वावलोकन केर उप्पर मंडराबू) "

#, fuzzy
#~| msgid "Install New Theme..."
#~ msgid "Get New Theme..."
#~ msgstr "नवीन प्रसंग संस्थापित करू..."

#, fuzzy
#~| msgid "Install New Theme..."
#~ msgid "Install From File..."
#~ msgstr "नवीन प्रसंग संस्थापित करू..."

#~ msgid "Remove Theme"
#~ msgstr "प्रसंग हटाबू"

#~ msgid "KDE Classic"
#~ msgstr "केडीई-क्लासिक"

#~ msgid "No description available"
#~ msgstr "केओ विवरण उपलब्ध नहि"

#~ msgid "Short"
#~ msgstr "छोट"

#~ msgid "Long"
#~ msgstr "नमहर"
