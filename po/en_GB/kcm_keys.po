# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-10 02:27+0000\n"
"PO-Revision-Date: 2023-02-27 21:03+0000\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: globalaccelmodel.cpp:197
#, kde-format
msgctxt ""
"%1 is the name of the component, %2 is the action for which saving failed"
msgid "Error while saving shortcut %1: %2"
msgstr "Error while saving shortcut %1: %2"

#: globalaccelmodel.cpp:309
#, kde-format
msgctxt "%1 is the name of an application"
msgid "Error while adding %1, it seems it has no actions."
msgstr "Error while adding %1, it seems it has no actions."

#: globalaccelmodel.cpp:352
#, kde-format
msgid "Error while communicating with the global shortcuts service"
msgstr "Error while communicating with the global shortcuts service"

#: kcm_keys.cpp:56
#, kde-format
msgid "Failed to communicate with global shortcuts daemon"
msgstr "Failed to communicate with global shortcuts daemon"

#: kcm_keys.cpp:297
#, kde-format
msgctxt "%2 is the name of a category inside the 'Common Actions' section"
msgid ""
"Shortcut %1 is already assigned to the common %2 action '%3'.\n"
"Do you want to reassign it?"
msgstr ""
"Shortcut %1 is already assigned to the common %2 action '%3'.\n"
"Do you want to reassign it?"

#: kcm_keys.cpp:301
#, kde-format
msgid ""
"Shortcut %1 is already assigned to action '%2' of %3.\n"
"Do you want to reassign it?"
msgstr ""
"Shortcut %1 is already assigned to action '%2' of %3.\n"
"Do you want to reassign it?"

#: kcm_keys.cpp:302
#, kde-format
msgctxt "@title:window"
msgid "Found conflict"
msgstr "Found conflict"

#: package/contents/ui/main.qml:27
#, kde-format
msgid "Applications"
msgstr "Applications"

#: package/contents/ui/main.qml:27
#, kde-format
msgid "Commands"
msgstr "Commands"

#: package/contents/ui/main.qml:27
#, kde-format
msgid "System Settings"
msgstr "System Settings"

#: package/contents/ui/main.qml:27
#, kde-format
msgid "Common Actions"
msgstr "Common Actions"

#: package/contents/ui/main.qml:51
#, kde-format
msgid "Cannot export scheme while there are unsaved changes"
msgstr "Cannot export scheme while there are unsaved changes"

#: package/contents/ui/main.qml:63
#, kde-format
msgid ""
"Select the components below that should be included in the exported scheme"
msgstr ""
"Select the components below that should be included in the exported scheme"

#: package/contents/ui/main.qml:69
#, kde-format
msgid "Save scheme"
msgstr "Save scheme"

#: package/contents/ui/main.qml:169
#, kde-format
msgctxt "@tooltip:button %1 is the text of a custom command"
msgid "Edit command for %1"
msgstr "Edit command for %1"

#: package/contents/ui/main.qml:185
#, kde-format
msgid "Remove all shortcuts for %1"
msgstr "Remove all shortcuts for %1"

#: package/contents/ui/main.qml:196
#, kde-format
msgid "Undo deletion"
msgstr "Undo deletion"

#: package/contents/ui/main.qml:247
#, kde-format
msgid "No items matched the search terms"
msgstr "No items matched the search terms"

#: package/contents/ui/main.qml:280
#, kde-format
msgid "Select an item from the list to view its shortcuts here"
msgstr "Select an item from the list to view its shortcuts here"

#: package/contents/ui/main.qml:305
#, kde-format
msgctxt "@action:button Keep translated text as short as possible"
msgid "Add Application…"
msgstr "Add Application…"

#: package/contents/ui/main.qml:315
#, kde-format
msgctxt "@action:button Keep translated text as short as possible"
msgid "Add Command…"
msgstr "Add Command…"

#: package/contents/ui/main.qml:334
#, kde-format
msgid "Import Scheme…"
msgstr "Import Scheme…"

#: package/contents/ui/main.qml:339
#, kde-format
msgid "Cancel Export"
msgstr "Cancel Export"

#: package/contents/ui/main.qml:339
#, kde-format
msgid "Export Scheme…"
msgstr "Export Scheme…"

#: package/contents/ui/main.qml:359
#, kde-format
msgid "Export Shortcut Scheme"
msgstr "Export Shortcut Scheme"

#: package/contents/ui/main.qml:359 package/contents/ui/main.qml:469
#, kde-format
msgid "Import Shortcut Scheme"
msgstr "Import Shortcut Scheme"

#: package/contents/ui/main.qml:361
#, kde-format
msgctxt "Template for file dialog"
msgid "Shortcut Scheme (*.kksrc)"
msgstr "Shortcut Scheme (*.kksrc)"

#: package/contents/ui/main.qml:389
#, kde-format
msgid "Edit Command"
msgstr "Edit Command"

#: package/contents/ui/main.qml:389
#, kde-format
msgid "Add Command"
msgstr "Add Command"

#: package/contents/ui/main.qml:402
#, kde-format
msgid "Save"
msgstr "Save"

#: package/contents/ui/main.qml:402
#, kde-format
msgid "Add"
msgstr "Add"

#: package/contents/ui/main.qml:427
#, kde-format
msgid "Enter a command or choose a script file:"
msgstr "Enter a command or choose a script file:"

#: package/contents/ui/main.qml:441
#, kde-format
msgctxt "@action:button"
msgid "Choose…"
msgstr "Choose…"

#: package/contents/ui/main.qml:454
#, kde-format
msgctxt "@title:window"
msgid "Choose Script File"
msgstr "Choose Script File"

#: package/contents/ui/main.qml:456
#, kde-format
msgctxt "Template for file dialog"
msgid "Script file (*.*sh)"
msgstr "Script file (*.*sh)"

#: package/contents/ui/main.qml:476
#, kde-format
msgid "Select the scheme to import:"
msgstr "Select the scheme to import:"

#: package/contents/ui/main.qml:490
#, kde-format
msgid "Custom Scheme"
msgstr "Custom Scheme"

#: package/contents/ui/main.qml:495
#, kde-format
msgid "Select File…"
msgstr "Select File…"

#: package/contents/ui/main.qml:495
#, kde-format
msgid "Import"
msgstr "Import"

#: package/contents/ui/ShortcutActionDelegate.qml:31
#, kde-format
msgid "Editing shortcut: %1"
msgstr "Editing shortcut: %1"

#: package/contents/ui/ShortcutActionDelegate.qml:43
#, kde-format
msgctxt ""
"%1 is the name action that is triggered by the key sequences following "
"after :"
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/ShortcutActionDelegate.qml:56
#, kde-format
msgid "No active shortcuts"
msgstr "No active shortcuts"

#: package/contents/ui/ShortcutActionDelegate.qml:96
#, kde-format
msgctxt "%1 decides if singular or plural will be used"
msgid "Default shortcut"
msgid_plural "Default shortcuts"
msgstr[0] "Default shortcut"
msgstr[1] "Default shortcuts"

#: package/contents/ui/ShortcutActionDelegate.qml:98
#, kde-format
msgid "No default shortcuts"
msgstr "No default shortcuts"

#: package/contents/ui/ShortcutActionDelegate.qml:106
#, kde-format
msgid "Default shortcut %1 is enabled."
msgstr "Default shortcut %1 is enabled."

#: package/contents/ui/ShortcutActionDelegate.qml:106
#, kde-format
msgid "Default shortcut %1 is disabled."
msgstr "Default shortcut %1 is disabled."

#: package/contents/ui/ShortcutActionDelegate.qml:127
#, kde-format
msgid "Custom shortcuts"
msgstr "Custom shortcuts"

#: package/contents/ui/ShortcutActionDelegate.qml:152
#, kde-format
msgid "Delete this shortcut"
msgstr "Delete this shortcut"

#: package/contents/ui/ShortcutActionDelegate.qml:158
#, kde-format
msgid "Add custom shortcut"
msgstr "Add custom shortcut"

#: package/contents/ui/ShortcutActionDelegate.qml:193
#, kde-format
msgid "Cancel capturing of new shortcut"
msgstr "Cancel capturing of new shortcut"

#: standardshortcutsmodel.cpp:34
#, kde-format
msgid "File"
msgstr "File"

#: standardshortcutsmodel.cpp:35
#, kde-format
msgid "Edit"
msgstr "Edit"

#: standardshortcutsmodel.cpp:37
#, kde-format
msgid "Navigation"
msgstr "Navigation"

#: standardshortcutsmodel.cpp:38
#, kde-format
msgid "View"
msgstr "View"

#: standardshortcutsmodel.cpp:39
#, kde-format
msgid "Settings"
msgstr "Settings"

#: standardshortcutsmodel.cpp:40
#, kde-format
msgid "Help"
msgstr "Help"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Steve Allewell"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "steve.allewell@gmail.com"

#~ msgid "Shortcuts"
#~ msgstr "Shortcuts"

#~ msgid "David Redondo"
#~ msgstr "David Redondo"
