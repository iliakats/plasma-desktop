# Bulgarian translations for plasma-desktop package.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Automatically generated, 2022.
# Mincho Kondarev <mkondarev@yahoo.de>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-08 02:19+0000\n"
"PO-Revision-Date: 2022-12-08 22:37+0100\n"
"Last-Translator: Mincho Kondarev <mkondarev@yahoo.de>\n"
"Language-Team: Bulgarian <kde-i18n-doc@kde.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.3\n"

#: kcmtouchscreen.cpp:44
#, kde-format
msgid "Automatic"
msgstr "Автоматично"

#: kcmtouchscreen.cpp:50
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr "%1 - (%2,%3 %4×%5)"

#: package/contents/ui/main.qml:29
#, kde-format
msgid "No touchscreens found"
msgstr "Не открит сензорен екран"

#: package/contents/ui/main.qml:45
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr "Устройство:"

#: package/contents/ui/main.qml:58
#, kde-format
msgid "Enabled:"
msgstr "Активирано:"

#: package/contents/ui/main.qml:66
#, kde-format
msgid "Target display:"
msgstr "Главен екран:"
