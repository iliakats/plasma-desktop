# translation of kcmkclock.po to esperanto
# Copyright (C) 2004, 2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the plasma-desktop package.
# Matthias Peick <matthias@peick.de>, 2004.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2007.
# Pierre-Marie Pédrot <pedrotpmx@wanadoo.fr>, 2007.
#
# Minuskloj: ĉ ĝ ĵ ĥ ŝ ŭ   Majuskloj: Ĉ Ĝ Ĵ Ĥ Ŝ Ŭ
#
msgid ""
msgstr ""
"Project-Id-Version: kcmkclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-02-27 00:49+0000\n"
"PO-Revision-Date: 2023-04-08 17:16+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Wolfram Diestel,Oliver Kellogg"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wolfram@steloj.de,okellogg@users.sourceforge.net"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: dateandtime.ui:22
#, kde-format
msgid "Date and Time"
msgstr "Dato kaj Tempo"

#. i18n: ectx: property (text), widget (QCheckBox, setDateTimeAuto)
#: dateandtime.ui:30
#, kde-format
msgid "Set date and time &automatically"
msgstr "Meti daton kaj tempon &aŭtomate"

#. i18n: ectx: property (text), widget (QLabel, timeServerLabel)
#: dateandtime.ui:53
#, kde-format
msgid "&Time server:"
msgstr "&Temposervilo:"

#. i18n: ectx: property (whatsThis), widget (KDatePicker, cal)
#: dateandtime.ui:86
#, kde-format
msgid "Here you can change the system date's day of the month, month and year."
msgstr ""
"Tie ĉi vi povas ŝanĝi la monatan tagon, monaton kaj jaron de la sistemdato."

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: dateandtime.ui:122
#, kde-format
msgid "Time Zone"
msgstr "Tempzono"

#. i18n: ectx: property (text), widget (QLabel, label)
#: dateandtime.ui:128
#, kde-format
msgid "To change the local time zone, select your area from the list below."
msgstr ""
"Por ŝanĝi la lokan tempozonon, elektu vian regionon de la malsupra listo"

#. i18n: ectx: property (text), widget (QLabel, m_local)
#: dateandtime.ui:151
#, kde-format
msgid "Current local time zone:"
msgstr "Aktuala loka tempozono:"

#. i18n: ectx: property (placeholderText), widget (KTreeWidgetSearchLine, tzonesearch)
#: dateandtime.ui:161
#, kde-format
msgid "Search…"
msgstr "Serĉi…"

#: dtime.cpp:61
#, kde-format
msgid ""
"No NTP utility has been found. Install 'ntpdate' or 'rdate' command to "
"enable automatic updating of date and time."
msgstr ""
"Neniu NTP-utilaĵo estis trovita. Instalu komandon 'ntpdate' aŭ 'rdate' por "
"ebligi aŭtomatan ĝisdatigon de dato kaj horo."

#: dtime.cpp:91
#, kde-format
msgid ""
"Here you can change the system time. Click into the hours, minutes or "
"seconds field to change the relevant value, either using the up and down "
"buttons to the right or by entering a new value."
msgstr ""
"Tie ĉi vi povas ŝanĝi la sistemtempon. Klaku al horoj, minutoj aŭ sekundoj "
"por ŝangi la koncernan valoron. Tion vi povas fari per la butonoj kun la "
"sagetoj aŭ per rekta entajpo."

#: dtime.cpp:113
#, kde-format
msgctxt "%1 is name of time zone"
msgid "Current local time zone: %1"
msgstr "Aktuala loka tempozono: %1"

#: dtime.cpp:116
#, kde-format
msgctxt "%1 is name of time zone, %2 is its abbreviation"
msgid "Current local time zone: %1 (%2)"
msgstr "Aktuala loka tempozono: %1 (%2)"

#: dtime.cpp:203
#, kde-format
msgid ""
"Public Time Server (pool.ntp.org),        asia.pool.ntp.org,        europe."
"pool.ntp.org,        north-america.pool.ntp.org,        oceania.pool.ntp.org"
msgstr ""
"Publika tempservilo (pool.ntp.org),        asia.pool.ntp.org,        europe."
"pool.ntp.org,        north-america.pool.ntp.org,        oceania.pool.ntp.org"

#: dtime.cpp:274
#, kde-format
msgid "Unable to contact time server: %1."
msgstr "Ne eblas kontakti temposervilon: %1."

#: dtime.cpp:278
#, kde-format
msgid "Can not set date."
msgstr "Ne eblas difini la daton."

#: dtime.cpp:281
#, kde-format
msgid "Error setting new time zone."
msgstr "Eraro dum metado de nova tempozono."

#: dtime.cpp:281
#, kde-format
msgid "Time zone Error"
msgstr "Tempzona eraro"

#: dtime.cpp:299
#, kde-format
msgid ""
"<h1>Date & Time</h1> This system settings module can be used to set the "
"system date and time. As these settings do not only affect you as a user, "
"but rather the whole system, you can only change these settings when you "
"start the System Settings as root. If you do not have the root password, but "
"feel the system time should be corrected, please contact your system "
"administrator."
msgstr ""
"<h1>Dato kaj Tempo</h1> Tiun modulon vi povas uzi por alĝustigi la sistemajn "
"daton kaj tempon. Ĉar tio koncernas ne nur vin kiel unuopa uzanto, sed la "
"tutan sistemon, vi povas ŝanĝi tion nur, se vi lanĉis la Stircentron kiel "
"sistemestro. Se vi ne havas pasvorton de sistemestro, sed la sistemtempo "
"estas malĝusta, bonvolu kontakti la sistemestron."

#: main.cpp:49
#, kde-format
msgid "KDE Clock Control Module"
msgstr "KDE agorda modulo por la horloĝo"

#: main.cpp:53
#, kde-format
msgid "(c) 1996 - 2001 Luca Montecchiani"
msgstr "(c) 1996 - 2001 Luca Montecchiani"

#: main.cpp:55
#, kde-format
msgid "Luca Montecchiani"
msgstr "Luca Montecchiani"

#: main.cpp:55
#, kde-format
msgid "Original author"
msgstr "Origina aŭtoro"

#: main.cpp:56
#, kde-format
msgid "Paul Campbell"
msgstr "Paul Campbell"

#: main.cpp:56
#, kde-format
msgid "Current Maintainer"
msgstr "Nuna fleganto"

#: main.cpp:57
#, kde-format
msgid "Benjamin Meyer"
msgstr "Benjamin Meyer"

#: main.cpp:57
#, kde-format
msgid "Added NTP support"
msgstr "Aldono de NTP-subteno"

#: main.cpp:60
#, kde-format
msgid ""
"<h1>Date & Time</h1> This control module can be used to set the system date "
"and time. As these settings do not only affect you as a user, but rather the "
"whole system, you can only change these settings when you start the System "
"Settings as root. If you do not have the root password, but feel the system "
"time should be corrected, please contact your system administrator."
msgstr ""
"<h1>Dato kaj Tempo</h1> Tiun modulon vi povas uzi por alĝustigi la sistemajn "
"daton kaj tempon. Ĉar tio koncernas ne nur vin kiel unuopa uzanto, sed la "
"tutan sistemon, vi povas ŝanĝi tion nur, se vi lanĉis la Stircentron kiel "
"sistemestro. Se vi ne havas pasvorton de sistemestro, sed la sistemtempo "
"estas malĝusta, bonvolu kontakti la sistemestron."

#: main.cpp:113
#, kde-format
msgid "Unable to authenticate/execute the action: %1, %2"
msgstr "Ne eblas aŭtentigi/plenumigi la agon: %1, %2"

#: main.cpp:133
#, kde-format
msgid "Unable to change NTP settings"
msgstr "Ne eblas ŝanĝi NTP-agordojn"

#: main.cpp:144
#, kde-format
msgid "Unable to set current time"
msgstr "Ne eblas meti aktualan tempon"

#: main.cpp:154
#, kde-format
msgid "Unable to set timezone"
msgstr "Ne eblas meti tempzonon"
