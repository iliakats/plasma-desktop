# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Marce Villarino <mvillarino@kde-espana.org>, 2014.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:33+0000\n"
"PO-Revision-Date: 2019-10-19 15:35+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: package/contents/ui/main.qml:57
#, kde-format
msgid ""
"This will disable file searching in KRunner and launcher menus, and remove "
"extended metadata display from all KDE applications."
msgstr ""

#: package/contents/ui/main.qml:66
#, kde-format
msgid ""
"Do you want to delete the saved index data? %1 of space will be freed, but "
"if indexing is re-enabled later, the entire index will have to be re-created "
"from scratch. This may take some time, depending on how many files you have."
msgstr ""

#: package/contents/ui/main.qml:68
#, kde-format
msgid "Delete Index Data"
msgstr ""

#: package/contents/ui/main.qml:88
#, kde-format
msgid "The system must be restarted before these changes will take effect."
msgstr ""

#: package/contents/ui/main.qml:92
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr ""

#: package/contents/ui/main.qml:99
#, fuzzy, kde-format
#| msgid ""
#| "File Search helps you quickly locate all your files based on their content"
msgid ""
"File Search helps you quickly locate all your files based on their content."
msgstr ""
"A busca de ficheiros permítelle dar rapidamente cos seus ficheiros segundo o "
"contido"

#: package/contents/ui/main.qml:106
#, kde-format
msgid "Enable File Search"
msgstr "Activar a busca de ficheiros"

#: package/contents/ui/main.qml:121
#, kde-format
msgid "Also index file content"
msgstr "Indexar tamén o contido do ficheiro."

#: package/contents/ui/main.qml:135
#, kde-format
msgid "Index hidden files and folders"
msgstr ""

#: package/contents/ui/main.qml:159
#, kde-format
msgid "Status: %1, %2% complete"
msgstr ""

#: package/contents/ui/main.qml:164
#, kde-format
msgid "Pause Indexer"
msgstr ""

#: package/contents/ui/main.qml:164
#, kde-format
msgid "Resume Indexer"
msgstr ""

#: package/contents/ui/main.qml:177
#, kde-format
msgid "Currently indexing: %1"
msgstr ""

#: package/contents/ui/main.qml:182
#, kde-format
msgid "Folder specific configuration:"
msgstr ""

#: package/contents/ui/main.qml:209
#, kde-format
msgid "Start indexing a folder…"
msgstr ""

#: package/contents/ui/main.qml:220
#, kde-format
msgid "Stop indexing a folder…"
msgstr ""

#: package/contents/ui/main.qml:271
#, kde-format
msgid "Not indexed"
msgstr ""

#: package/contents/ui/main.qml:272
#, kde-format
msgid "Indexed"
msgstr ""

#: package/contents/ui/main.qml:302
#, kde-format
msgid "Delete entry"
msgstr ""

#: package/contents/ui/main.qml:317
#, fuzzy, kde-format
#| msgid "Select the folder which should be excluded"
msgid "Select a folder to include"
msgstr "Escolla o cartafol que se debe excluír"

#: package/contents/ui/main.qml:317
#, fuzzy, kde-format
#| msgid "Select the folder which should be excluded"
msgid "Select a folder to exclude"
msgstr "Escolla o cartafol que se debe excluír"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Marce Villarino"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mvillarino@kde-espana.org"

#~ msgid "File Search"
#~ msgstr "Busca de ficheiros"

#~ msgid "Copyright 2007-2010 Sebastian Trüg"
#~ msgstr "Copyright 2007-2010 Sebastian Trüg"

#~ msgid "Sebastian Trüg"
#~ msgstr "Sebastian Trüg"

#~ msgid "Vishesh Handa"
#~ msgstr "Vishesh Handa"

#~ msgid "Tomaz Canabrava"
#~ msgstr "Tomaz Canabrava"

#, fuzzy
#~| msgid "Folder %1 is already excluded"
#~ msgid "%1 is excluded."
#~ msgstr "O cartafol %1 xa está excluído"

#~ msgid "Do not search in these locations:"
#~ msgstr "Non buscar nestes lugares:"

#~ msgid ""
#~ "Not allowed to exclude root folder, please disable File Search if you do "
#~ "not want it"
#~ msgstr ""
#~ "Non se permite excluír o cartafol raíz, desactive a busca de ficheiros se "
#~ "non o quere."

#~ msgid "Folder's parent %1 is already excluded"
#~ msgstr "O %1 pai do cartafol xa está excluído"

#~ msgid "Configure File Search"
#~ msgstr "Configurar a busca de ficheiros"

#~ msgid "The root directory is always hidden"
#~ msgstr "O directorio raíz sempre está escollido"
