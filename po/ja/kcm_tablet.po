msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:33+0000\n"
"PO-Revision-Date: 2021-12-16 09:16-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: kcmtablet.cpp:32
#, kde-format
msgid "Primary (default)"
msgstr ""

#: kcmtablet.cpp:33
#, kde-format
msgid "Portrait"
msgstr ""

#: kcmtablet.cpp:34
#, kde-format
msgid "Landscape"
msgstr ""

#: kcmtablet.cpp:35
#, kde-format
msgid "Inverted Portrait"
msgstr ""

#: kcmtablet.cpp:36
#, kde-format
msgid "Inverted Landscape"
msgstr ""

#: kcmtablet.cpp:86
#, kde-format
msgid "Follow the active screen"
msgstr ""

#: kcmtablet.cpp:94
#, kde-format
msgctxt "model - (x,y widthxheight)"
msgid "%1 - (%2,%3 %4×%5)"
msgstr ""

#: kcmtablet.cpp:137
#, kde-format
msgid "Fit to Output"
msgstr ""

#: kcmtablet.cpp:138
#, kde-format
msgid "Fit Output in tablet"
msgstr ""

#: kcmtablet.cpp:139
#, kde-format
msgid "Custom size"
msgstr ""

#: package/contents/ui/main.qml:32
#, kde-format
msgid "No drawing tablets found."
msgstr ""

#: package/contents/ui/main.qml:40
#, kde-format
msgctxt "@label:listbox The device we are configuring"
msgid "Device:"
msgstr ""

#: package/contents/ui/main.qml:78
#, kde-format
msgid "Target display:"
msgstr ""

#: package/contents/ui/main.qml:97
#, kde-format
msgid "Orientation:"
msgstr ""

#: package/contents/ui/main.qml:109
#, kde-format
msgid "Left-handed mode:"
msgstr ""

#: package/contents/ui/main.qml:119
#, kde-format
msgid "Area:"
msgstr ""

#: package/contents/ui/main.qml:207
#, kde-format
msgid "Resize the tablet area"
msgstr ""

#: package/contents/ui/main.qml:231
#, kde-format
msgctxt "@option:check"
msgid "Lock aspect ratio"
msgstr ""

#: package/contents/ui/main.qml:239
#, kde-format
msgctxt "tablet area position - size"
msgid "%1,%2 - %3×%4"
msgstr ""

#: package/contents/ui/main.qml:247
#, kde-format
msgid "Tool Button 1"
msgstr ""

#: package/contents/ui/main.qml:248
#, kde-format
msgid "Tool Button 2"
msgstr ""

#: package/contents/ui/main.qml:249
#, kde-format
msgid "Tool Button 3"
msgstr ""

#: package/contents/ui/main.qml:292
#, kde-format
msgctxt "@label:listbox The pad we are configuring"
msgid "Pad:"
msgstr ""

#: package/contents/ui/main.qml:303
#, kde-format
msgid "None"
msgstr ""

#: package/contents/ui/main.qml:325
#, kde-format
msgid "Button %1:"
msgstr ""
