# Translation of kcm_landingpage.po to Catalan
# Copyright (C) 2021-2022 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-09 02:10+0000\n"
"PO-Revision-Date: 2022-02-12 10:54+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Generator: Lokalize 20.12.0\n"

#. i18n: ectx: label, entry (colorScheme), group (General)
#: landingpage_kdeglobalssettings.kcfg:9
#, kde-format
msgid "Color scheme name"
msgstr "Nom de l'esquema de color"

#. i18n: ectx: label, entry (singleClick), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:15
#, kde-format
msgid "Single click to open files"
msgstr "Clic únic per a obrir fitxers"

#. i18n: ectx: label, entry (lookAndFeelPackage), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:19
#, kde-format
msgid "Global Look and Feel package"
msgstr "Paquet d'aspecte i comportament global"

#. i18n: ectx: label, entry (defaultLightLookAndFeel), group (KDE)
#. i18n: ectx: label, entry (defaultDarkLookAndFeel), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:23
#: landingpage_kdeglobalssettings.kcfg:27
#, kde-format
msgid "Global Look and Feel package, alternate"
msgstr "Paquet d'aspecte i comportament global, alternatiu"

#. i18n: ectx: label, entry (animationDurationFactor), group (KDE)
#: landingpage_kdeglobalssettings.kcfg:31
#, kde-format
msgid "Animation speed"
msgstr "Velocitat de l'animació"

#: package/contents/ui/main.qml:27
#, kde-format
msgid "Theme:"
msgstr "Tema:"

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Animation speed:"
msgstr "Velocitat de l'animació:"

#: package/contents/ui/main.qml:93
#, kde-format
msgctxt "Animation speed"
msgid "Slow"
msgstr "Lenta"

#: package/contents/ui/main.qml:99
#, kde-format
msgctxt "Animation speed"
msgid "Instant"
msgstr "Instantània"

#: package/contents/ui/main.qml:113
#, kde-format
msgid "Change Wallpaper…"
msgstr "Canviar el fons de pantalla…"

#: package/contents/ui/main.qml:120
#, kde-format
msgid "More Appearance Settings…"
msgstr "Més opcions d'aparença…"

#: package/contents/ui/main.qml:135
#, kde-format
msgctxt ""
"part of a sentence: 'Clicking files or folders [opens them/selects them]'"
msgid "Clicking files or folders:"
msgstr "En fer clic a fitxers o carpetes:"

#: package/contents/ui/main.qml:136
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders opens them'"
msgid "Opens them"
msgstr "S'obren"

#: package/contents/ui/main.qml:141
#, kde-format
msgid "Select by clicking on item's selection marker"
msgstr "Selecciona en clicar al marcador de selecció de l'element"

#: package/contents/ui/main.qml:162
#, kde-format
msgctxt "part of a sentence: 'Clicking files or folders selects them'"
msgid "Selects them"
msgstr "Se seleccionen"

#: package/contents/ui/main.qml:167
#, kde-format
msgid "Open by double-clicking instead"
msgstr "Obre fent doble clic"

#: package/contents/ui/main.qml:190
#, kde-format
msgid "More Behavior Settings…"
msgstr "Més opcions de comportament…"

#: package/contents/ui/main.qml:202
#, kde-format
msgid "Most Used Pages:"
msgstr "Pàgines més usades:"
