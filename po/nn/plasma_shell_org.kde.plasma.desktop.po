# Translation of plasma_shell_org.kde.plasma.desktop to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-03 02:18+0000\n"
"PO-Revision-Date: 2023-03-04 15:44+0100\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.2\n"

#: contents/activitymanager/ActivityItem.qml:212
msgid "Currently being used"
msgstr "Vert brukt no"

#: contents/activitymanager/ActivityItem.qml:252
msgid ""
"Move to\n"
"this activity"
msgstr ""
"Flytt til denne\n"
"aktiviteten"

#: contents/activitymanager/ActivityItem.qml:282
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"Vis òg i denne\n"
"aktiviteten"

#: contents/activitymanager/ActivityItem.qml:344
msgid "Configure"
msgstr "Set opp"

#: contents/activitymanager/ActivityItem.qml:363
msgid "Stop activity"
msgstr "Stopp aktivitet"

#: contents/activitymanager/ActivityList.qml:143
msgid "Stopped activities:"
msgstr "Stoppa aktivitetar:"

#: contents/activitymanager/ActivityManager.qml:127
msgid "Create activity…"
msgstr "Legg til aktivitet …"

#: contents/activitymanager/Heading.qml:61
msgid "Activities"
msgstr "Aktivitetar"

#: contents/activitymanager/StoppedActivityItem.qml:135
msgid "Configure activity"
msgstr "Set opp aktivitet"

#: contents/activitymanager/StoppedActivityItem.qml:152
msgid "Delete"
msgstr "Slett"

#: contents/applet/AppletError.qml:123
msgid "Sorry! There was an error loading %1."
msgstr "Det oppstod dessverre ein feil ved lasting av %1."

#: contents/applet/AppletError.qml:161
msgid "Copy to Clipboard"
msgstr "Kopier til utklippstavla"

#: contents/applet/AppletError.qml:184
msgid "View Error Details…"
msgstr "Vis feildetaljar …"

#: contents/applet/CompactApplet.qml:71
msgid "Open %1"
msgstr "Opna %1"

#: contents/configuration/AboutPlugin.qml:19
#: contents/configuration/AppletConfiguration.qml:248
msgid "About"
msgstr "Om"

#: contents/configuration/AboutPlugin.qml:47
msgid "Send an email to %1"
msgstr "Send e-post til %1"

#: contents/configuration/AboutPlugin.qml:61
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "Opna heime­side: %1"

#: contents/configuration/AboutPlugin.qml:125
msgid "Copyright"
msgstr "Opphavsrett"

#: contents/configuration/AboutPlugin.qml:145 contents/explorer/Tooltip.qml:93
msgid "License:"
msgstr "Lisens:"

#: contents/configuration/AboutPlugin.qml:148
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "Vis lisenstekst"

#: contents/configuration/AboutPlugin.qml:160
msgid "Authors"
msgstr "Opphavspersonar"

#: contents/configuration/AboutPlugin.qml:170
msgid "Credits"
msgstr "Bidragsytarar"

#: contents/configuration/AboutPlugin.qml:181
msgid "Translators"
msgstr "Omsetjarar"

#: contents/configuration/AboutPlugin.qml:195
msgid "Report a Bug…"
msgstr "Meld frå om feil …"

#: contents/configuration/AppletConfiguration.qml:56
msgid "Keyboard Shortcuts"
msgstr "Snøggtastar"

#: contents/configuration/AppletConfiguration.qml:295
msgid "Apply Settings"
msgstr "Bruk innstillingar"

#: contents/configuration/AppletConfiguration.qml:296
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"Innstillingane i denne modulen er endra. Ønskjer du å bruka eller forkasta "
"endringane?"

#: contents/configuration/AppletConfiguration.qml:326
msgid "OK"
msgstr "OK"

#: contents/configuration/AppletConfiguration.qml:334
msgid "Apply"
msgstr "Bruk"

#: contents/configuration/AppletConfiguration.qml:340
#: contents/explorer/AppletAlternatives.qml:179
msgid "Cancel"
msgstr "Avbryt"

#: contents/configuration/ConfigCategoryDelegate.qml:28
msgid "Open configuration page"
msgstr "Opnar oppsettsida"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Venstreknappen"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Høgreknappen"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Midtknappen"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Tilbakeknapp"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Framknapp"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Loddrett rulling"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Vassrett rulling"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr " + "

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr "Om"

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Legg til handling"

#: contents/configuration/ConfigurationContainmentAppearance.qml:78
msgid "Layout changes have been restricted by the system administrator"
msgstr "Utformingsendringar er låste av systemadministratoren"

#: contents/configuration/ConfigurationContainmentAppearance.qml:93
msgid "Layout:"
msgstr "Utforming:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:107
msgid "Wallpaper type:"
msgstr "Type bakgrunn:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:127
msgid "Get New Plugins…"
msgstr "Hent nye programtillegg …"

#: contents/configuration/ConfigurationContainmentAppearance.qml:195
msgid "Layout changes must be applied before other changes can be made"
msgstr "Du må ta i bruk utformingsendringane før du kan gjera andre endringar"

#: contents/configuration/ConfigurationContainmentAppearance.qml:199
msgid "Apply Now"
msgstr "Bruk no"

#: contents/configuration/ConfigurationShortcuts.qml:16
msgid "Shortcuts"
msgstr "Snarvegar"

#: contents/configuration/ConfigurationShortcuts.qml:28
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr "Denne snøggtasten vil starta skjermelementet som om du trykte på det."

#: contents/configuration/ContainmentConfiguration.qml:29
msgid "Wallpaper"
msgstr "Bakgrunnsbilete"

#: contents/configuration/ContainmentConfiguration.qml:34
msgid "Mouse Actions"
msgstr "Musehandlingar"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Inndata her"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:39
#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:194
#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Panel"
msgstr "Fjern panelet"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:47
msgid "Panel Alignment"
msgstr "Panelplassering"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:63
msgid "Top"
msgstr "Oppe"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:63
msgid "Left"
msgstr "Til venstre"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:73
msgid "Center"
msgstr "I midten"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:83
msgid "Bottom"
msgstr "Nede"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:83
msgid "Right"
msgstr "Til høgre"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:93
msgid "Visibility"
msgstr "Synlegheit"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:107
msgid "Always Visible"
msgstr "Alltid synleg"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:114
msgid "Auto Hide"
msgstr "Gøym automatisk"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:121
msgid "Windows Can Cover"
msgstr "Vindauge kan dekkja over"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:128
msgid "Windows Go Below"
msgstr "Vindauge kan liggja under"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:137
msgid "Opacity"
msgstr "Gjennomsikt"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:154
msgid "Adaptive"
msgstr "Adaptiv"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:161
msgid "Opaque"
msgstr "Ugjennomsiktig"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:168
msgid "Translucent"
msgstr "Gjennomsiktig"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:176
msgid "Maximize Panel"
msgstr "Maksimer panelet"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:184
msgid "Floating Panel"
msgstr "Flytande panel"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:200
msgid "Shortcut"
msgstr "Snarveg"

#: contents/configuration/panelconfiguration/MoreSettingsMenu.qml:213
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr "Bruk snøggtasten til å flytta fokuset til panelet"

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum height."
msgstr "Dra for å endra makshøgda."

#: contents/configuration/panelconfiguration/Ruler.qml:20
msgid "Drag to change maximum width."
msgstr "Dra for å endra maksbreidda."

#: contents/configuration/panelconfiguration/Ruler.qml:20
#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Double click to reset."
msgstr "Dobbeltklikka for å tilbakestilla."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum height."
msgstr "Dra for å endra minimumshøgda."

#: contents/configuration/panelconfiguration/Ruler.qml:21
msgid "Drag to change minimum width."
msgstr "Dra for å endra minimumsbreidda."

#: contents/configuration/panelconfiguration/Ruler.qml:69
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"Dra for å endra plassering på denne skjermkanten.\n"
"Dobbeltklikk for å tilbakestilla."

#: contents/configuration/panelconfiguration/ToolBar.qml:24
msgid "Add Widgets…"
msgstr "Legg til element …"

#: contents/configuration/panelconfiguration/ToolBar.qml:25
msgid "Add Spacer"
msgstr "Legg til mellomrom"

#: contents/configuration/panelconfiguration/ToolBar.qml:26
msgid "More Options…"
msgstr "Fleire val …"

#: contents/configuration/panelconfiguration/ToolBar.qml:222
msgctxt "Minimize the length of this string as much as possible"
msgid "Drag to move"
msgstr "Dra for å flytta"

#: contents/configuration/panelconfiguration/ToolBar.qml:261
msgctxt "@info:tooltip"
msgid "Use arrow keys to move the panel"
msgstr "Bruk piltastane til å flytta panelet"

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel width:"
msgstr "Panelbreidd:"

#: contents/configuration/panelconfiguration/ToolBar.qml:282
msgid "Panel height:"
msgstr "Panelhøgd:"

#: contents/configuration/panelconfiguration/ToolBar.qml:402
#: contents/configuration/ShellContainmentConfiguration.qml:43
msgid "Close"
msgstr "Lukk"

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "Handsaming av panel og skrivebord"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr "Du kan dra panela og skriveborda for å flytta dei mellom skjermane."

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:178
msgid "Swap with Desktop on Screen %1"
msgstr "Byt med skrivebordet på skjerm %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Move to Screen %1"
msgstr "Flytt til skjerm %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:193
msgid "Remove Desktop"
msgstr "Fjern skrivebord"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:267
msgid "%1 (primary)"
msgstr "%1 (hovudskjerm)"

#: contents/explorer/AppletAlternatives.qml:63
msgid "Alternative Widgets"
msgstr "Alternative element"

#: contents/explorer/AppletAlternatives.qml:163
msgid "Switch"
msgstr "Byt"

#: contents/explorer/AppletDelegate.qml:177
msgid "Undo uninstall"
msgstr "Angra avinstallering"

#: contents/explorer/AppletDelegate.qml:178
msgid "Uninstall widget"
msgstr "Avinstaller element"

#: contents/explorer/Tooltip.qml:102
msgid "Author:"
msgstr "Laga av:"

#: contents/explorer/Tooltip.qml:110
msgid "Email:"
msgstr "E-postadresse:"

#: contents/explorer/Tooltip.qml:129
msgid "Uninstall"
msgstr "Avinstaller"

#: contents/explorer/WidgetExplorer.qml:129
#: contents/explorer/WidgetExplorer.qml:240
msgid "All Widgets"
msgstr "Alle element"

#: contents/explorer/WidgetExplorer.qml:194
msgid "Widgets"
msgstr "Skjermelement"

#: contents/explorer/WidgetExplorer.qml:202
msgid "Get New Widgets…"
msgstr "Hent nye element …"

#: contents/explorer/WidgetExplorer.qml:251
msgid "Categories"
msgstr "Kategoriar"

#: contents/explorer/WidgetExplorer.qml:331
msgid "No widgets matched the search terms"
msgstr "Ingen element passar til søkjeteksten"

#: contents/explorer/WidgetExplorer.qml:331
msgid "No widgets available"
msgstr "Ingen element er tilgjengelege"
