# Translation of kcm_krunnersettings to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2015, 2018, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-02 00:50+0000\n"
"PO-Revision-Date: 2022-06-18 23:41+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.04.2\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/ui/main.qml:29
#, kde-format
msgid "Position on screen:"
msgstr "Plassering:"

#: package/contents/ui/main.qml:33
#, kde-format
msgid "Top"
msgstr "Øvst"

#: package/contents/ui/main.qml:40
#, kde-format
msgid "Center"
msgstr "I midten"

#: package/contents/ui/main.qml:53
#, kde-format
msgid "Activation:"
msgstr "Start:"

#: package/contents/ui/main.qml:56
#, kde-format
msgctxt "@option:check"
msgid "Activate when pressing any key on the desktop"
msgstr "Start ved tastetrykk på skrivebordet"

#: package/contents/ui/main.qml:69
#, kde-format
msgid "History:"
msgstr "Historikk:"

#: package/contents/ui/main.qml:72
#, kde-format
msgctxt "@option:check"
msgid "Remember past searches"
msgstr "Hugs tidlegare søk"

#: package/contents/ui/main.qml:83
#, kde-format
msgctxt "@option:check"
msgid "Retain last search when re-opening"
msgstr "Ta fram førre søk ved gjenopning"

#: package/contents/ui/main.qml:94
#, kde-format
msgctxt "@option:check"
msgid "Activity-aware (last search and history)"
msgstr "Aktivitetsbasert (førre søk og fillogg)"

#: package/contents/ui/main.qml:109
#, kde-format
msgid "Clear History"
msgstr "Tøm loggen"

#: package/contents/ui/main.qml:110
#, kde-format
msgctxt "@action:button %1 activity name"
msgid "Clear History for Activity \"%1\""
msgstr "Tøm loggen for aktiviteten «%1»"

#: package/contents/ui/main.qml:111
#, kde-format
msgid "Clear History…"
msgstr "Tøm loggen …"

#: package/contents/ui/main.qml:140
#, kde-format
msgctxt "@item:inmenu delete krunner history for all activities"
msgid "For all activities"
msgstr "For alle aktivitetane"

#: package/contents/ui/main.qml:153
#, kde-format
msgctxt "@item:inmenu delete krunner history for this activity"
msgid "For activity \"%1\""
msgstr "For aktiviteten «%1»"

#: package/contents/ui/main.qml:170
#, kde-format
msgctxt "@label"
msgid "Plugins:"
msgstr "Programtillegg:"

#: package/contents/ui/main.qml:174
#, kde-format
msgctxt "@action:button"
msgid "Configure Enabled Search Plugins…"
msgstr "Set opp påslegne søkjetillegg …"
